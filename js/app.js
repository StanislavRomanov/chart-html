'use strict';

var ChartApp = angular.module('ChartApp', [
  'ui.bootstrap'
]);

function MainCtrl($scope, Charts) {
  $scope.maxChartsPerPage = 5;
  $scope.maxPages = 5;
  $scope.chartsCurrPage = 1;
  $scope.chartSearchString = '';
  $scope.order = 'chart_position';
  $scope.reverseOrder = false;
  $scope.sortingTypes = [
    {
      title: 'Track Name',
      active: false
    },
    {
      title: 'Chart',
      active: true
    },
    {
      title: 'Play',
      active: false
    },
    {
      title: 'Date Added',
      active: false
    },
    {
      title: 'Popularity',
      active: false
    }
  ];

  Charts.getFromJSON({
    limit: 5,
    offset: 0,
    search: ''
  })
    .then(function(result) {
      $scope.charts = result.charts;
      $scope.totalChartsCount = result.totalChartsCount;
    })
    .catch(function(err) {
      console.log(err);
    });

  $scope.loadCharts = function() {
    Charts.getFromJSON({
      limit: $scope.maxChartsPerPage,
      offset: ($scope.chartsCurrPage - 1) * $scope.maxChartsPerPage,
      search: $scope.chartSearchString,
      sorting: $scope.order,
      reverseOrder: $scope.reverseOrder
    })
      .then(function(result) {
        $scope.charts = result.charts;
        $scope.totalChartsCount = result.totalChartsCount;
      })
      .catch(function(err) {
        console.log(err);
      });
  };

  $scope.sortBy = function(sortingType) {
    $scope.sortingTypes.forEach(function(type) { type.active = false; });
    sortingType.active = true;
    switch (sortingType.title) {
      case 'Track Name': $scope.order = 'track.title'; $scope.reverseOrder = false; break;
      case 'Chart': $scope.order = 'chart_position'; $scope.reverseOrder = false; break;
      case 'Play': $scope.order = 'track.plays'; $scope.reverseOrder = true; break;
      case 'Date Added': $scope.order = 'track.date_added'; $scope.reverseOrder = true; break;
      case 'Popularity': $scope.order = 'track.likes'; $scope.reverseOrder = true; break;
    }
    $scope.loadCharts();
  }
}

function Decorate($provide) {
  $provide.decorator('paginationDirective', function($delegate) {
    var directive = $delegate[0];

    directive.templateUrl = "paginationOverride.tpl.html";

    return $delegate;
  });
}

ChartApp
  .config(['$provide', Decorate])
  .controller('MainCtrl', ['$scope', 'Charts', MainCtrl]);
