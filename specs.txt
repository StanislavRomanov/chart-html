Reorder - can reorder by track name, chart position, number of plays, date added and number of likes.
Filter (Search) - can filter by track name
Pagination - display max 5 chart items at any time
Track card - all entries will have a track and a video. Some will have a third track.

Full list
Filtered list (as at top - each will be a list item, e.g. Genre could be "Pop" and "Rock")
Paginated list
Ordered list

Each item will have the following:

Track Image
Track Title
Track Artist
Track Tags
Likes / Plays / Track Length

Track information (Click on title) - this can be loaded via a separate API, but will include:
Comments
Description
Ratings
Video
Another optional track with similar information (all from same API)

The track information includes an MP3 that can be played. We already have this functionality.
